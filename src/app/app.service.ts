import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  dataURL = "https://api.myjson.com/bins/kez8a";
  
  constructor(private http: HttpClient) { 
    this.http = http;
  }

  getAllData(): Observable<any>{
    return this.http.get(this.dataURL);
  }
}
