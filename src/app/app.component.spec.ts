import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './component/header/header.component';
import { SearchComponent } from './component/search/search.component';
import { JobListingComponent } from './component/job-listing/job-listing.component';
import { FooterComponent } from './component/footer/footer.component';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { ListComponent } from './component/job-listing/list/list.component';
import { SpinnerComponent } from './component/spinner/spinner.component';
import { HttpClientModule } from '@angular/common/http';


describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule
      ],
      declarations: [
        AppComponent,
        HeaderComponent,
        SearchComponent,
        JobListingComponent,
        FooterComponent,
        ListComponent,
        SpinnerComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

});
