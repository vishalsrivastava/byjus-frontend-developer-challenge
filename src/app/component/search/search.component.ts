import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AppService } from 'src/app/app.service';
import { Utils } from 'src/app/utils/search';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  showLoader : boolean = false;
  @Output() responseData= new EventEmitter<any>();

  searchCriteria = {
    experience: "",
    location: "",
    skills: ""
  };

  searchForm = new FormGroup({
    experienceControl: new FormControl(''),
    locationControl: new FormControl(''),
    skillControl: new FormControl('')
  });

  constructor(private service: AppService) { }

  ngOnInit() {
    
  }
  getData(){
    this.showLoader = true;
    this.service.getAllData().subscribe((response) => {
      
      //Setting data that is fetched from search form
      this.searchCriteria.experience = (this.searchForm.get('experienceControl').value.toLowerCase());
      this.searchCriteria.location = (this.searchForm.get('locationControl').value.toLowerCase());
      this.searchCriteria.skills = (this.searchForm.get('skillControl').value.toLowerCase());
      //Emitting response to App Component
      this.responseData.emit(Utils.searchByCriteria(this.searchCriteria,response.jobsfeed));
      this.showLoader = false;
    });

  }
}
