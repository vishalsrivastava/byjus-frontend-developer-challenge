import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobListingComponent } from './job-listing.component';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { ListComponent } from '../../component/job-listing/list/list.component';
import { SpinnerComponent } from '../../component/spinner/spinner.component';

describe('JobListingComponent', () => {
  let component: JobListingComponent;
  let fixture: ComponentFixture<JobListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobListingComponent,ListComponent,SpinnerComponent ],
      imports:[ReactiveFormsModule,FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
