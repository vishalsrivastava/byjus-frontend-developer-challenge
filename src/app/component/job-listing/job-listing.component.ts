import { Component, OnInit, Input } from '@angular/core';
import { Utils } from 'src/app/utils/search';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-job-listing',
  templateUrl: './job-listing.component.html',
  styleUrls: ['./job-listing.component.scss']
})
export class JobListingComponent implements OnInit {

  // Taking input from App Component & will be manipulated accordingly
  @Input() data: any;
  sortData: any;
  showRecords: boolean = false;
  originalArray: any;

  //Creation of form group for filter input form
  filterForm = new FormGroup({
    companyControl: new FormControl('')
  });
  
  constructor() { }

  ngOnInit() {
    if(this.data){
      this.originalArray = this.data.slice();
      this.showRecords = true;
    }
  }

  // Function to sort according to key passed like location or experience
  sortBy(key: any){
    
    this.data = Utils.sortByKey(this.data,key);
  }

  // Function to filter data according to company name
  filterBy(){
    this.data = Utils.filterByCompany(this.data, this.filterForm.get('companyControl').value.toLowerCase());
    if(!this.filterForm.get('companyControl').value){
      this.data = this.originalArray;
    }
  }

}
