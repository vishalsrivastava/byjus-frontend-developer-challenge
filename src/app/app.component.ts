import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  jobListingFlag : boolean = false;
  showLoader : boolean = false;
  data : any;

  setResponseData(data: any){
    if(data){
      this.data = data;
      console.log(data+ " nnn");
      this.jobListingFlag = true;
    }
  }
}
