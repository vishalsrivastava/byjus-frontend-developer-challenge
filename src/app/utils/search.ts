export class Utils{

    static searchByCriteria(criteria: any, responseData: any): any{

        return responseData.filter((item) => {
            return (criteria.location ===""? true : (item.location.toLowerCase().search(criteria.location.toLowerCase())!=-1)) 
            && (criteria.skills ===""? true : (item.skills.toLowerCase().search(criteria.skills.toLowerCase())!=-1))
            && (criteria.experience ==="" ? true : 
              (item.experience.toLowerCase().search(criteria.experience.toLowerCase())!=-1)) ;
             
        });
    }

    static sortByKey(array, key): any{
        return array.sort((a,b)=>{
            let x = a[key];
            let y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    }

    static filterByCompany(array, key): any{
        if(!key){
            return array;
        }
        return array.filter((item) => {
            return item.companyname.toLowerCase().search(key)!=-1;
        });
    }

} 